import React, { Component } from 'react';
import ContactUs from './ContactUs';
import Home from './HomeComponent';
import Help from './Help';
import ProblemsAndSuggestions from './ProblemsAndSuggestions';
import AboutUs from './AboutUs';
import Animals from './Animals';
import HelpSeekers from './HelpSeekers';
import Jobs from './Jobs';
import Servises from './Servises';
import Machines from './Machines';
import SecondHand from './SecondHand';
import Accessories from './Accessories';
import Veichal from './Veichal';
import RealEstate from './RealEstate';
import ProductsAndServieses from './ProductsAndServieses';
import GiveAd from './GiveAd';
import MyPage from './MyPage';
import { View, Platform } from 'react-native';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements';

const ContactUsNavigator = createStackNavigator({
    ContactUs: { screen: ContactUs }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const HelpNavigator = createStackNavigator({
    Help: { screen: Help }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const ProblemsAndSuggestionsNavigator = createStackNavigator({
    ProblemsAndSuggestions: { screen: ProblemsAndSuggestions }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const AboutUsNavigator = createStackNavigator({
    AboutUs: { screen: AboutUs }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const AnimalsNavigator = createStackNavigator({
    Animals: { screen: Animals }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const HelpSeekersNavigator = createStackNavigator({
    HelpSeekers: { screen: HelpSeekers }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const JobsNavigator = createStackNavigator({
    Jobs: { screen: Jobs }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const ServisesNavigator = createStackNavigator({
    Servises: { screen: Servises }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const MachinesNavigator = createStackNavigator({
    Machines: { screen: Machines }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const SecondHandNavigator = createStackNavigator({
    SecondHand: { screen: SecondHand }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const AccessoriesNavigator = createStackNavigator({
    Accessories: { screen: Accessories }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const VeichalNavigator = createStackNavigator({
    Veichal: { screen: Veichal }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const RealEstateNavigator = createStackNavigator({
    RealEstate: { screen: RealEstate }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const ProductsAndServiesesNavigator = createStackNavigator({
    ProductsAndServieses: { screen: ProductsAndServieses }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const GiveAdNavigator = createStackNavigator({
    GiveAd: { screen: GiveAd }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const MyPageNavigator = createStackNavigator({
    MyPage: { screen: MyPage }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const HomeNavigator = createStackNavigator({
    Home: { screen: Home }
  }, {
    navigationOptions: ({ navigation }) => ({
      headerStyle: {
          backgroundColor: "#512DA8"
      },
      headerTitleStyle: {
          color: "#fff"            
      },
      headerTintColor: "#fff",
	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
              onPress={ () => navigation.toggleDrawer() }/>
    })
});
const MainNavigator = createDrawerNavigator({
    Home: 
      { screen: HomeNavigator,
        navigationOptions: {
          title: 'Home',
          drawerLabel: 'Home'
        }
      },
	  MyPage: 
      { screen: MyPageNavigator,
        navigationOptions: {
          title: 'MyPage',
          drawerLabel: 'MyPage'
        }
      },
	  GiveAd: 
      { screen: GiveAdNavigator,
        navigationOptions: {
          title: 'Give an ad',
          drawerLabel: 'Give an ad'
        }
      },
	  ProductsAndServieses: 
      { screen: ProductsAndServiesesNavigator,
        navigationOptions: {
          title: 'Products and Servises',
          drawerLabel: 'Products and Servises'
        }
      },
	  RealEstate: 
      { screen: RealEstateNavigator,
        navigationOptions: {
          title: 'RealEstate',
          drawerLabel: 'RealEstate'
        }
      },
	  Veichal: 
      { screen: VeichalNavigator,
        navigationOptions: {
          title: 'Veichal',
          drawerLabel: 'Veichal'
        }
      },
	  Accessories: 
      { screen: AccessoriesNavigator,
        navigationOptions: {
          title: 'Accessories',
          drawerLabel: 'Accessories'
        }
      },
	  SecondHand: 
      { screen: SecondHandNavigator,
        navigationOptions: {
          title: 'SecondHand',
          drawerLabel: 'SecondHand'
        }
      },
	  Machines: 
      { screen: MachinesNavigator,
        navigationOptions: {
          title: 'Machines',
          drawerLabel: 'Machines'
        }
      },
	  Servises: 
      { screen: ServisesNavigator,
        navigationOptions: {
          title: 'Servises',
          drawerLabel: 'Servises'
        }
      },
	  Jobs: 
      { screen: JobsNavigator,
        navigationOptions: {
          title: 'Jobs',
          drawerLabel: 'Jobs'
        }
      },
	  HelpSeekers: 
      { screen: HelpSeekersNavigator,
        navigationOptions: {
          title: 'Help Seekers',
          drawerLabel: 'Help Seekers'
        }
      },
	  Animals: 
      { screen: AnimalsNavigator,
        navigationOptions: {
          title: 'Animals',
          drawerLabel: 'Animals'
        }
      },
	  AboutUs: 
      { screen: AboutUsNavigator,
        navigationOptions: {
          title: 'About us',
          drawerLabel: 'About us'
        }
      },
	  ProblemsAndSuggestions: 
      { screen: ProblemsAndSuggestionsNavigator,
        navigationOptions: {
          title: 'Problems and suggestions',
          drawerLabel: 'Problems and suggestions'
        }
      },
	  Help: 
      { screen: HelpNavigator,
        navigationOptions: {
          title: 'Help',
          drawerLabel: 'Help'
        }
      },
	  ContactUs: 
      { screen: ContactUsNavigator,
        navigationOptions: {
          title: 'Contact Us',
          drawerLabel: 'Contact Us'
        }
      },
    navigationOptions: ({ navigation }) => ({
	  	   headerLeft: <Icon name="menu" size={24} 
              color= 'white'
	  onPress={ () => navigation.toggleDrawer() }/>})
	  
}, {
  drawerBackgroundColor: '#D1C4E9'
});
class Main extends Component {
	

  render() {
 
    return (
	
        <View style={{flex:1 }}>
            <MainNavigator />
        </View>
    );
  }
}
  
export default Main;