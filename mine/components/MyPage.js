import React, { Component } from 'react';
import { Text, View } from 'react-native';



class MyPage extends Component {

    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        title: 'MyPage',
    };

    render() {
        
        return(
            <View>
			<Text>MyPage</Text>
            </View>
        );
    }
}

export default MyPage;